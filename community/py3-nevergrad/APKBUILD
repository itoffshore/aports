# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-nevergrad
pkgver=0.7.0
pkgrel=0
pkgdesc="A Python toolbox for performing gradient-free optimization"
url="https://github.com/facebookresearch/nevergrad"
arch="noarch !s390x" #py3-bayesian-optimization
license="MIT"
depends="python3 py3-bayesian-optimization py3-cma py3-numpy py3-pandas py3-typing-extensions"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-pytest-cov"
subpackages="$pkgname-pyc"
source="https://github.com/facebookresearch/nevergrad/archive/$pkgver/nevergrad-$pkgver.tar.gz"
builddir="$srcdir/nevergrad-$pkgver"
options="!check" # several test dependencies are missing | skip tests for now

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
26fb1268a9a7de5c8c6b6cd30e6280a28781176984917fbde1b8a9c50fa8d34b6a874b72d04bbd8c09a2c4f16b5251581f15a83113a789785a84d4b4aa86882b  nevergrad-0.7.0.tar.gz
"
