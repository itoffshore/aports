# Maintainer: psykose <alice@ayaya.dev>
pkgname=py3-pyproject-api
pkgver=1.5.2
pkgrel=0
pkgdesc="Python API to interact with the python pyproject.toml based projects"
url="https://github.com/tox-dev/pyproject-api"
arch="noarch"
license="MIT"
depends="py3-packaging"
makedepends="
	py3-gpep517
	py3-hatch-vcs
	py3-hatchling
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-pytest-mock
	py3-virtualenv
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/tox-dev/pyproject-api/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/pyproject-api-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
42ffa87a1e35e3271293edf6c221d0d6ad5c3c0cbe61be2ffa9805753185089ad57ff2615c0d9663efe146b4091bb20f57367c2152f4aa7c1f4d24ae1c1c4159  py3-pyproject-api-1.5.2.tar.gz
"
