# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-tempora
pkgver=5.3.0
pkgrel=0
pkgdesc="Objects and routines pertaining to date and time (tempora)"
url="https://github.com/jaraco/tempora"
arch="noarch"
license="MIT"
depends="
	py3-jaraco.functools
	py3-tz
	python3
	"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest py3-freezegun py3-pytest-freezegun"
subpackages="$pkgname-pyc"
source="https://pypi.python.org/packages/source/t/tempora/tempora-$pkgver.tar.gz"
builddir="$srcdir/tempora-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION="$pkgver"
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/tempora-$pkgver-py3-none-any.whl
}

sha512sums="
0bc82318e275792e0ad7d56ac46b78fd6e75b8fc3b6a57b7138ed7800a2f9f8ef7e64c855bc54ac48e190bfd05e9ea3397b45544e4dcfb2e2905946d76bb2622  tempora-5.3.0.tar.gz
"
